class Node():

    def __init__(self,data):
        self.data = data
        self.next = None

#Bağlı liste sınıfı



class LinkedList():

    def __init__(self):
        self.head = None   # head of linkedlist

    def countList(self):
        temp = self.head
        count =0

        while temp:
            count +=1
            temp = temp.next
        return count
    """
    def content(self):
        temp = self.head
        list = []
        while temp:
            list.append(temp)
            temp = temp.next
            print()

        return list
    """

    def push(self, new_data):

        new_node = Node(new_data) # create new node

        new_node.next = self.head # make next of new node as head

        self.head = new_node  # move the head to point to new Node

    def search(self, x):

        current = self.head

        # loop till current not equal to None
        while current != None:
            if current.data == x:
                return True  # data found

            current = current.next

        return False  #if data not found